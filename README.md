# ros-workshop-cru

These are the files that are used in the workshop.


## Pre-work:

If you are a Windows 10 user:

### 1)Install Virtual box from : https://www.virtualbox.org/
Oracle VM VirtualBox
VirtualBox is a powerful x86 and AMD64/Intel64 virtualization product for enterprise as well as home use. Not only is VirtualBox an extremely feature rich, high performance product for enterprise customers, it is also the only professional solution that is freely available as Open Source Software under the terms of the GNU General Public License (GPL) version 2.
www.virtualbox.org

### 2)Then install a version of Ubuntu operating system within the Virtual machine you have created in the previous step:
Ubuntu 16.04 or 18.04 should be fine:  https://ubuntu.com/download/desktop
Download Ubuntu Desktop | Download | Ubuntu
Download the latest LTS version of Ubuntu, for desktop PCs and laptops. LTS stands for long-term support — which means five years, until April 2023, of free security and maintenance updates, guaranteed.
ubuntu.com

Then follow the setup with Ubuntu!

https://www.wikihow.com/Install-Ubuntu-on-VirtualBox
	
How to Install Ubuntu on VirtualBox (with Pictures) - wikiHow
How to Install Ubuntu on VirtualBox. This wikiHow teaches you how to install Ubuntu Linux on a computer by using VirtualBox. VirtualBox is a program which allows you to install an operating system without changing your computer's main...
www.wikihow.com

### 3)After this Point we would be able to use the OS and work with ROS Melodic!

### 4) I will provide installation instructions with a script file to make life easier and accelerate development!

Note: Please verify that you can login and use the machine accordingly as it would be difficult to debug issues online independently.
See you soon!