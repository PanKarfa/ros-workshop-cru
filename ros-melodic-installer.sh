#!/usr/bin/bash
#While executing this script do not use sudo otherwise you will need to fix the permissions of rosdep, plus recreate the workspace for the current user

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
wait
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
wait
sudo apt-get update


wait
echo "Which setup do you want to install (1:ros-desktop-full,2:ros-desktop,3:ros-base)?"
read OPTION

case $OPTION in
        1)
        sudo apt-get install ros-melodic-desktop-full
        ;;

        2)
        sudo apt-get install ros-melodic-desktop
        ;;

        3)
        sudo apt-get install ros-melodic-ros-base
        ;;
esac
wait

sudo rosdep init
wait
rosdep update
wait
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc

source ~/.bashrc

sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
wait
mkdir catkin_ws
wait
mkdir catkin_ws/src
wait
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
echo "export ROS_WORKSPACE=$HOME/catkin_ws" >> ~/.bashrc

source ~/.bashrc